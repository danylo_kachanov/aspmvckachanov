﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace AspMvcKachanov.Providers
{
    public class HttpRequestsProvider
    {
        #region Instance
        private HttpRequestsProvider() { }

        private static HttpRequestsProvider csvDataPovider;
        public static HttpRequestsProvider Instance
        {
            get
            {
                if (csvDataPovider == null)
                {
                    csvDataPovider = new HttpRequestsProvider();
                }

                return csvDataPovider;
            }
        }

        #endregion

        public async Task<string> GetCsvData(string url)
        {
            var content = string.Empty;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(url);   
            if (response.IsSuccessStatusCode)
            {
                content = response.Content.ReadAsStringAsync().Result;
            }
            else
            {
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
            }

            return content;
        }
    }
}