﻿using AspMvcKachanov.Extensions;
using AspMvcKachanov.Models;
using System.Collections.Generic;

namespace AspMvcKachanov.Providers
{
    public class ParseDataProvider
    {
        #region Instance
        private ParseDataProvider() { }

        private static ParseDataProvider jsonPovider;
        public static ParseDataProvider Instance
        {
            get
            {
                if (jsonPovider == null)
                {
                    jsonPovider = new ParseDataProvider();
                }

                return jsonPovider;
            }
        }

        #endregion

        public List<AttendanceRecord> ParseCsvData(string content, List<AttendanceRecord> users)
        {
            //TODO: need correct json file or correct parse logic

            //var csvFieldNames = new string[] { "id", "date", "time", "status", "tabId", "fullName", "department", "position", "entryPoint", "accessZone" };

            var csvElements = content.Split('\n');
            var metaInfo = csvElements.Slice(0, 8);
            var tableHeaderInfo = csvElements.Slice(9);
            var dataRecords = csvElements.Slice(10, csvElements.Length-1);

            foreach (var record in dataRecords)
            {
                var fields = record.Split(',');

                if (fields.Length >= 10)
                {
                    users.Add(new AttendanceRecord
                    {
                        Id = fields[0].Replace(" ",""),
                        Date = fields[1],
                        Time = fields[2],
                        Status = fields[3],
                        TabId = fields[4],
                        FullName = fields[5],
                        Department = fields[6],
                        Position = fields[7],
                        EntryPoint = fields[8],
                        AccessZone = fields[9]
                    });
                }
            }

            return users;
        }
    }
}